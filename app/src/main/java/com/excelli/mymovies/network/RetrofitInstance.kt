package com.excelli.mymovies.network

import com.excelli.mymovies.BuildConfig
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory


object RetrofitInstance {
    private var retrofit: Retrofit? = null
    private val BASE_URL = "http://www.omdbapi.com"

    /**
     * Create an instance of Retrofit object
     */
    private fun getRetrofitInstance(): Retrofit? {
        retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(
                OkHttpClient.Builder()
                    .addInterceptor { chain ->
                        val url = chain
                            .request().url
                            .newBuilder()
                            .addQueryParameter("apikey", BuildConfig.SECRET_API_KEY)
                            .build()
                        chain.proceed(chain.request().newBuilder().url(url).build())
                    }
                    .addInterceptor(getLoggerInterceptor())
                    .build()
            )
            .addConverterFactory(
                MoshiConverterFactory.create(
                    Moshi.Builder().add(KotlinJsonAdapterFactory()).build()
                )
            )
            .build()
        return retrofit
    }

    private fun getLoggerInterceptor(): HttpLoggingInterceptor {
        val logging = HttpLoggingInterceptor()
        logging.level = (HttpLoggingInterceptor.Level.BODY)
        return logging
    }

    fun getRetrofitAPI() = getRetrofitInstance()?.create(MovieApiService::class.java)

}