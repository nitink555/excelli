package com.excelli.mymovies.network.models

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class MoviesResponse(
    @Json(name = "Search")
    val searchResults: List<SearchMovies>,

    @Json(name = "Response")
    val response: String,

    @Json(name = "totalResults")
    val totalResults: String
)