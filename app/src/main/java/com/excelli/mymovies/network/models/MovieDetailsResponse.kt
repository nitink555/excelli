package com.excelli.mymovies.network.models

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class MovieDetailsResponse(
    @Json(name = "Title")
    val title: String,

    @Json(name = "Year")
    val year: String,

    @Json(name = "Poster")
    val poster: String,

    @Json(name = "imdbRating")
    val rating: String,

    @Json(name = "Plot")
    val plot: String,

    @Json(name = "Response")
    val response: String
)