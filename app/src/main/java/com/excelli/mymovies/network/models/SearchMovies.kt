package com.excelli.mymovies.network.models

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class SearchMovies(
    @Json(name = "Title")
    val title: String,

    @Json(name = "Year")
    val year: String,

    @Json(name = "imdbID")
    val ID: String,

    @Json(name = "Type")
    val type: String,

    @Json(name = "Poster")
    val posterUrl: String
)