package com.excelli.mymovies.network

import com.excelli.mymovies.network.models.MovieDetailsResponse
import com.excelli.mymovies.network.models.MoviesResponse
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface MovieApiService {
    @GET("/?")
    suspend fun getListOfMovies(@Query("s") searchString: String): Response<MoviesResponse>

    @GET("/?")
    suspend fun getMoviewDetails(@Query("i") id: String): Response<MovieDetailsResponse>
}