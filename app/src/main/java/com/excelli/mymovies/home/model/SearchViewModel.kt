package com.excelli.mymovies.home.model

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.ViewModel
import com.excelli.mymovies.home.repositories.MoviesListRepository
import com.excelli.mymovies.network.models.MoviesResponse
import com.excelli.mymovies.util.Resource
import retrofit2.Response


class SearchViewModel(private val searchString: String) : ViewModel() {

    private val searchRepository: MoviesListRepository = MoviesListRepository()

    private val moviesListResponse = MediatorLiveData<Resource<Response<MoviesResponse>>?>()

    init {
        getMoviesListCall()
    }

    private fun getMoviesListCall() {
        moviesListResponse.addSource(searchRepository.getMoviesCall(searchString = searchString)) {
            moviesListResponse.value = it
        }
    }

    fun getMovies(): LiveData<Resource<Response<MoviesResponse>>?> {
        return moviesListResponse
    }
}