package com.excelli.mymovies.home.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.excelli.mymovies.R
import com.excelli.mymovies.databinding.FragmentMovieDetailBinding
import com.excelli.mymovies.home.model.MovieDetailsViewModel
import com.excelli.mymovies.network.models.MovieDetailsResponse
import com.excelli.mymovies.home.model.factories.DetailsViewModelFactory
import com.excelli.mymovies.util.Status

class MovieDetailFragment : Fragment() {

    /* Navgraph parameters for this fragment from ListFragment */
    private val args: MovieDetailFragmentArgs by navArgs()
    private lateinit var binding: FragmentMovieDetailBinding
    private lateinit var movieDetailsViewModel: MovieDetailsViewModel


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMovieDetailBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        /* adding notification bar flags which are used to hide it */
        requireActivity().window.addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)
        movieDetailsViewModel = ViewModelProvider(
            this,
            DetailsViewModelFactory(
                id = args.movieID
            )
        ).get(MovieDetailsViewModel::class.java)
        if ((activity as MainActivity).isNetworkConnected()) {
            movieDetailsViewModel.getMovieDetails()
                .observe(viewLifecycleOwner) {
                    it.let { resource ->
                        when (resource?.status) {
                            Status.SUCCESS -> resource.data?.let {
                                resource.data.body()?.let { it1 -> assignData(it1) }
                            }
                            Status.ERROR -> resource?.message?.let { it1 ->
                                showError(
                                    title = getString(
                                        R.string.error
                                    ), message = it1
                                )
                            }
                            Status.LOADING -> showProgressBar()
                            else -> resource?.message?.let { it1 ->
                                showError(
                                    title = getString(R.string.error),
                                    message = it1
                                )
                            }
                        }

                    }
                }
        } else {
            showError(
                title = getString(R.string.no_internet_title),
                message = getString(R.string.no_internet_message)
            )
        }
    }


    override fun onDestroy() {
        super.onDestroy()
       /* clearing notification bar flags which are used to hide it */
        requireActivity().window.clearFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)
    }

    private fun assignData(details: MovieDetailsResponse) {
        hideProgressBar()
        Glide.with(requireContext()).load(details.poster).centerCrop().into(binding.posterLarge) // using Glide to load the image
        binding.movieTitle.text = details.title
        binding.movieDesc.text = details.plot
        binding.imdbRating.text = details.rating + "/10"
    }

    private fun showError(title: String, message: String) {
        hideProgressBar()
        AlertDialog.Builder(requireContext()).setTitle(title)
            .setMessage(message)
            .setPositiveButton(android.R.string.ok) { _, _ -> }
            .setIcon(android.R.drawable.ic_dialog_alert).show()
    }

    private fun showProgressBar() {
        binding.progressBar.visibility = View.VISIBLE
    }

    private fun hideProgressBar() {
        binding.progressBar.visibility = View.GONE
    }
}