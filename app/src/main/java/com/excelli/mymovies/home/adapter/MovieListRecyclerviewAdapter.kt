package com.excelli.mymovies.home.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.excelli.mymovies.R
import com.excelli.mymovies.home.ui.MoviesListFragmentDirections
import com.excelli.mymovies.network.models.SearchMovies

class MovieListRecyclerviewAdapter(private val data: List<SearchMovies>) :
    RecyclerView.Adapter<MovieListRecyclerviewAdapter.MyViewHolder>() {

    class MyViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

        fun bind(property: SearchMovies) {
            val title = view.findViewById<TextView>(R.id.movieTitle)
            val imageView = view.findViewById<ImageView>(R.id.thumbnail)
            val description = view.findViewById<TextView>(R.id.movie_desc)

            title.text = property.title
            description.text = property.type

            Glide.with(view.context).load(property.posterUrl).centerCrop().into(imageView)

            view.setOnClickListener {
                val direction =
                    MoviesListFragmentDirections.actionMoviesListFragmentToMovieDetailFragment(
                        movieID = property.ID
                    )
                it.findNavController().navigate(direction)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.movie_list_row, parent, false)
        return MyViewHolder(v)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(data[position])
    }

}