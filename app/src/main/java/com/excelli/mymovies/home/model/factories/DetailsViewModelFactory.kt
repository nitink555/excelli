package com.excelli.mymovies.home.model.factories

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.excelli.mymovies.home.model.MovieDetailsViewModel

class DetailsViewModelFactory(private val id: String = "") : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MovieDetailsViewModel::class.java)) {
            return MovieDetailsViewModel(id = id) as T
        }
        throw IllegalArgumentException("Unknown class name")
    }

}