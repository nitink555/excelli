package com.excelli.mymovies.home.model.factories

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.excelli.mymovies.home.model.SearchViewModel

class ViewModelFactory(private val searchString: String = "") : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(SearchViewModel::class.java)) {
            return SearchViewModel(searchString = searchString) as T
        }
        throw IllegalArgumentException("Unknown class name")
    }

}