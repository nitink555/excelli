package com.excelli.mymovies.home.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.excelli.mymovies.R
import com.excelli.mymovies.home.model.factories.ViewModelFactory
import com.excelli.mymovies.databinding.FragmentMoviesListBinding
import com.excelli.mymovies.home.adapter.MovieListRecyclerviewAdapter
import com.excelli.mymovies.home.model.SearchViewModel
import com.excelli.mymovies.network.models.MoviesResponse
import com.excelli.mymovies.util.Status


class MoviesListFragment : Fragment() {

    private lateinit var binding: FragmentMoviesListBinding
    private lateinit var searchViewModel: SearchViewModel
    private lateinit var manager: RecyclerView.LayoutManager
    private lateinit var myAdapter: RecyclerView.Adapter<*>


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMoviesListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.toolBarLayout?.homeupBtn?.setOnClickListener {
            requireActivity().finish()
        }

        searchViewModel = ViewModelProvider(
            this,
            ViewModelFactory(
                searchString = "friends"
            )
        ).get(SearchViewModel::class.java)
        if ((activity as MainActivity).isNetworkConnected()) {
            searchViewModel.getMovies()
                .observe(viewLifecycleOwner) {
                    it.let { resource ->
                        when (resource?.status) {
                            Status.SUCCESS -> resource.data?.let {
                                resource.data.body()?.let { it1 -> updateList(it1) }
                            }
                            Status.ERROR -> resource?.message?.let { it1 ->
                                showError(
                                    title = getString(
                                        R.string.error
                                    ), message = it1
                                )
                            }
                            Status.LOADING -> showProgressBar()
                            else -> resource?.message?.let { it1 ->
                                showError(
                                    title = getString(R.string.error),
                                    message = it1
                                )
                            }
                        }

                    }
                }
        } else {
            showError(
                title = getString(R.string.no_internet_title),
                message = getString(R.string.no_internet_message)
            )
        }
    }


    private fun updateList(response: MoviesResponse) {
        hideProgressBar()
        manager = LinearLayoutManager(requireContext())
        binding.moviesListRv.apply {
            myAdapter = MovieListRecyclerviewAdapter(response.searchResults)
            layoutManager = manager
            adapter = myAdapter
        }
    }

    private fun showError(title: String, message: String) {
        hideProgressBar()
        AlertDialog.Builder(requireContext()).setTitle(title)
            .setMessage(message)
            .setPositiveButton(android.R.string.ok) { _, _ -> }
            .setIcon(android.R.drawable.ic_dialog_alert).show()
    }

    private fun showProgressBar() {
        binding.progressBar.visibility = View.VISIBLE
    }

    private fun hideProgressBar() {
        binding.progressBar.visibility = View.GONE
    }
}