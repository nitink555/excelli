package com.excelli.mymovies.home.repositories

import androidx.lifecycle.liveData
import com.excelli.mymovies.network.RetrofitInstance
import com.excelli.mymovies.util.Resource
import com.excelli.mymovies.util.ResultType
import kotlinx.coroutines.Dispatchers


class MoviesListRepository {
    fun getMoviesCall(searchString: String) =
        liveData(Dispatchers.IO) {
            emit(Resource.loading(data = null))
            try {
                val response =
                    RetrofitInstance.getRetrofitAPI()?.getListOfMovies(searchString = searchString)
                if ((response != null && response.isSuccessful
                            && response.errorBody() == null
                            && response.body() != null) && Resource.checkForError(responseCode = response.code()) != ResultType.ERROR
                ) {
                    emit(Resource.success(data = response))
                } else {
                    emit(Resource.error(data = null, message = "Error Occurred!"))
                }
            } catch (exception: Exception) {
                emit(exception.message?.let { Resource.error(data = null, message = it) })
            }
        }
}