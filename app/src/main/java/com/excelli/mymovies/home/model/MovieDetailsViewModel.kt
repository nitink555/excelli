package com.excelli.mymovies.home.model

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.ViewModel
import com.excelli.mymovies.home.repositories.MovieDetailsRepository
import com.excelli.mymovies.network.models.MovieDetailsResponse
import com.excelli.mymovies.util.Resource
import retrofit2.Response

class MovieDetailsViewModel(private val id: String) : ViewModel() {

    private val movieDetailsRepository = MovieDetailsRepository()
    private val moviesDetailResponse = MediatorLiveData<Resource<Response<MovieDetailsResponse>>?>()

    init {
        getMoviesListCall()
    }

    private fun getMoviesListCall() {
        moviesDetailResponse.addSource(movieDetailsRepository.getMovieDetails(idString = id)) {
            moviesDetailResponse.value = it
        }
    }

    fun getMovieDetails(): LiveData<Resource<Response<MovieDetailsResponse>>?> {
        return moviesDetailResponse
    }

}