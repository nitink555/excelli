package com.excelli.mymovies.util

data class Resource<out T>(val status: Status, val data: T?, val message: String?) {
    companion object {
        fun <T> success(data: T): Resource<T> =
            Resource(status = Status.SUCCESS, data = data, message = null)

        fun <T> error(data: T?, message: String): Resource<T> =
            Resource(status = Status.ERROR, data = data, message = message)

        fun <T> loading(data: T?): Resource<T> =
            Resource(status = Status.LOADING, data = data, message = null)

        fun checkForError(responseCode: Int): ResultType =
            when {
                responseCode < 100
                        || (responseCode in 300..399)
                        || (responseCode in 400..499)
                        || (responseCode in 500..599) -> ResultType.ERROR
                else -> ResultType.SUCCESS
            }
    }
}

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}

enum class ResultType {
    ERROR,
    SUCCESS
}